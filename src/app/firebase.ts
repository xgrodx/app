import * as firebase from 'firebase';
import 'firebase/firestore';
import "firebase/auth";

// Optionally import the services that you want to use
//import "firebase/database";
//import "firebase/functions";
//import "firebase/storage";

// Initialize Firebase
(() => {
    const firebaseConfig = {
        apiKey: "AIzaSyCoj1vhEQLgB2IdqzGsbizr8_AOvmcApNk",
        authDomain: "tomorrow-machine.firebaseapp.com",
        databaseURL: "https://tomorrow-machine.firebaseio.com",
        projectId: "tomorrow-machine",
        storageBucket: "tomorrow-machine.appspot.com",
        messagingSenderId: "912556447407",
        appId: "1:912556447407:web:a7801c025b7b5ab4daa65a"
      }

    if (firebase.apps.length === 0) {
        firebase.initializeApp(firebaseConfig)
    }
})()

export const auth = firebase.auth
export const firestore = firebase.firestore()
