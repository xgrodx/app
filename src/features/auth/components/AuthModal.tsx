import React from 'react'
import { View, Text } from "react-native"
import { Modal } from '../../common/components/Modal'
import { SignInForm } from './SignInForm'

export const AuthModal = ({navigation}) => {

    const handleLogin = () => {
        navigation.goBack()
    }

    return (
        <Modal navigation={navigation}>
            <Text style={{ fontSize: 30, marginBottom: 30 }}>Sign in</Text>
            <SignInForm onLogin={handleLogin} />
        </Modal>
    )
}