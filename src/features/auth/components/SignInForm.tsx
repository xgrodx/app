import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native'
import { Form } from '../../common/components/Form'
import { loginWithFacebook, loginWithGoogle } from '../../auth/flows/user'

export const SignInForm = ({onLogin}) => {

    const [loading, setLoading] = useState(false)

    const handleFacebookLogin = async () => {
        setLoading(true)
        const fb = await loginWithFacebook()
        if (fb) {
            onLogin()
        }
        setLoading(false)
    }

    const handleGoogleLogin = async () => {
        setLoading(true)
        const res = await loginWithGoogle()
        if (res) onLogin()
        setLoading(false)
    }

    const validateContent = () => {

    }

    const validateLength = () => {

    }

    return (
        <View>

            <View style={styles.socialButtons}>
                <TouchableOpacity onPress={handleFacebookLogin} style={[styles.button, styles.facebook]}>
                    <Text style={styles.buttonText}>Facebook</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={handleGoogleLogin} style={[styles.button, styles.google]}>
                    <Text style={styles.buttonText}>Google</Text>
                </TouchableOpacity>
            </View>

            <Text style={{fontStyle: 'italic', textAlign: 'center', marginVertical: 15}}>or</Text>

            <Form
                fields={
                    {
                    firstName: {
                        label: 'First Name'
                    },
                    email: {
                        label: 'Email',
                        validators: [validateContent],
                        inputProps: {
                            keyboardType: 'email-address',
                        }
                    },
                    password: {
                        label: 'Password',
                        validators: [validateContent, validateLength],
                        inputProps: {
                            secureTextEntry: true
                        }
                    }
                    }
                }
            />

        </View>
    )
}

const styles = StyleSheet.create({
    button: {
        padding: 10,
        backgroundColor: '#ff0000',
        borderRadius: 5,
        marginBottom: 15,
    },
    buttonText: {
        color: 'white',
        fontWeight: '400',
        fontSize: 16,
        textAlign: 'center'

    },
    facebook: {
        backgroundColor: '#4267B2',
    },
    google: {
        backgroundColor: '#4285F4',
    }
})