import { auth } from '../../../app/firebase'
import create from 'zustand'
import * as Facebook from 'expo-facebook'
// import * as Google from 'expo-google-sign-in'
import * as Google from 'expo-google-app-auth'

const FACEBOOK_APP_ID = '823739894847086'

export const userStore = create(set => ({
    user: undefined
}))
// Listen for authentication state to change.
auth().onAuthStateChanged(response => {
    const { setState } = userStore

    if (response != null) {
        setState({user: response})
        // Do other stuff
    } else {
        setState({user: undefined})
        // Clean up
    }
})
  
export const logOut = () => {
    console.log('Logging out')
    auth().signOut()
}

export const signUpWithEmail = (email, password) => {
    try {
        const response = auth().createUserWithEmailAndPassword(email, password)
        const { setState } = userStore
        setState({ user: response })
    } catch (e) {
        const { code, message } = e
    }
}

export async function loginWithFacebook() {
    await Facebook.initializeAsync(FACEBOOK_APP_ID);

    const { type, token } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ['public_profile'],
    });

    if (type === 'success') {
        // Build Firebase credential with the Facebook access token.
        const credential = auth.FacebookAuthProvider.credential(token);

        // Sign in with credential from the Facebook user.
        try {
            return await auth()
                .signInWithCredential(credential)
        }
        catch (err) {
            console.error('Could not log in with Facebook', err)
            return undefined
        }
    }
    return undefined
}

export async function loginWithGoogle() {
    console.log('Logging in with google')
    try {
        const result = await Google.logInAsync({
            behavior: 'web',
            scopes: ['profile', 'email'],
            // clientId: '912556447407-0cr4qidqvb0m9o223ivvf7o39uudpu6v.apps.googleusercontent.com',
            clientId: '912556447407-1a3evkb1bploliiufohs6etj8u3kbs9m.apps.googleusercontent.com',
            iosClientId: '912556447407-1a3evkb1bploliiufohs6etj8u3kbs9m.apps.googleusercontent.com',
            // redirectUrl: 'https://tomorrow-machine.firebaseapp.com/__/auth/handler'
            // NnrJ1_UG3gTRTigP5DJ0mtIt
        })
        const { accessToken, idToken } = result
        const credential = auth.GoogleAuthProvider.credential(idToken, accessToken)
        return await auth()
            .signInWithCredential(credential)
    } catch (err) {
        console.error('Could not log in with Google', err)
        return undefined
    }
    return undefined
}



export const getActivity = () => [

    // Mock data
    {
        name: 'Coffee',
        image: require('../../../../assets/images/coffee.png'),
        restaurant: 'Landet',
        credits: 3,
        date: '2 days ago'
    },
    {
        name: 'Lunch box',
        image: require('../../../../assets/images/lunch-box.png'),
        restaurant: 'Svenska Sushiköket',
        credits: 5,
        date: '3 days ago'
    },
    {
        name: 'Lunch box',
        image: require('../../../../assets/images/lunch-box.png'),
        restaurant: 'Hemköp Telefonplan',
        credits: 5,
        date: '5 days ago'
    },
    {
        name: 'Coffee',
        image: require('../../../../assets/images/coffee.png'),
        restaurant: 'Pressbyrån Telefonplan',
        credits: 3,
        date: 'a week ago'
    },
    {
        name: 'Coffee',
        image: require('../../../../assets/images/coffee.png'),
        restaurant: 'Stora Coop Västberga',
        credits: 3,
        date: 'a week ago'
    },
]