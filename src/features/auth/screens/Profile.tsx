import * as React from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { userStore, logOut } from '../flows/user'

export default function ProfileScreen({navigation}) {
    const user = userStore(state => state.user)
    return (
        <View style={styles.container}>
            { user 
            ?
                <>
                    <Text style={styles.header}>{user.displayName}</Text>
                    <TouchableOpacity style={styles.button} onPress={logOut}>
                        <Text style={styles.buttonText}>Log out</Text>
                    </TouchableOpacity>
                </>
            :
                <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Modal')}>
                    <Text style={styles.buttonText}>Sign in</Text>
                </TouchableOpacity>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 10
    },
    header: {
        fontSize: 30,
        marginBottom: 10,
    },
    button: {
        backgroundColor: '#518F81',
        padding: 10,
        borderRadius: 5, 
    },
    buttonText: {
        fontSize: 20,
        fontWeight: '400',
        textAlign: 'center',
        color: '#ffffff'
    }
})


