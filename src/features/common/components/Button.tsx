import * as React from 'react'
import { Button as ReactButton } from "react-native";

export default function Button({title}: {title:string}) {
    return (
        <ReactButton title={title} color="#449CB2" />
    )
}