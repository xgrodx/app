import React, { useState } from 'react';
import { Text, TextInput, View, StyleSheet } from 'react-native';
import { Input } from './Input'

const getInitialState = (fieldKeys) => {
  const state = {};
  fieldKeys.forEach((key) => {
    state[key] = '';
  });

  return state;
};

export const Form = ({ fields }) => {
  const fieldKeys = Object.keys(fields);
  const [values, setValues] = useState(getInitialState(fieldKeys));

  const onChangeValue = (key, value) => {
    const newState = { ...values, [key]: value };
    setValues(newState);
  };

  return (
    <>
      {fieldKeys.map((key) => {
        const field = fields[key];
        return (
          <View key={key} style={styles.formControl}>
              <Text style={styles.label}>{field.label}</Text>
              <Input
                {...field.inputProps}
                value={values[key]}
                onChangeText={(text) => onChangeValue(key, text)}
              />
              {/* <TextInput
                {...field.inputProps}
                value={values[key]}
                onChangeText={(text) => onChangeValue(key, text)}
                style={styles.inputField}
              /> */}
          </View>
        )})}
    </>
  )
};

const styles = StyleSheet.create({
  formControl: {
    marginBottom: 15,
  },
  inputField: {
    width: '100%',
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 5,
    padding: 10
  },
  label: {
    fontWeight: '600',
    marginBottom: 10 
  }
})