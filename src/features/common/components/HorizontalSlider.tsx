import { useLinkProps } from '@react-navigation/native'
import * as React from 'react'
import { FlatList, View, Text, Image, StyleSheet } from 'react-native'
import * as faker from 'faker'
import Star from '../../home/components/Star'
import { useVendorsStore } from '../../vendors/flows/vendors'
import { LinearGradient } from 'expo-linear-gradient'
import { useEffect } from 'react'

const renderCard = ({ item }) => (
    <View style={styles.cardWrapper}>
        <View style={styles.card}>
            <Image style={styles.image} source={{uri: item.image}} />
            {/* <View style={{position: 'absolute', top: 5, right: 5}}>
                <Star width={45} height={45} value={95} />
            </View> */}
            <View style={styles.nameOverlay}>
                <LinearGradient
                    // Background Linear Gradient
                    colors={['transparent', '#000']}
                    start={{x: 1, y: 0}}
                    end={{x: 1, y: 1}}
                    style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                    }}
                />
                <Text style={styles.name}>{item.name}</Text>
            </View>
            <View style={styles.credits}>
                <LinearGradient
                    // Background Linear Gradient
                    colors={['#000', 'transparent']}
                    start={{x: 1, y: 0}}
                    end={{x: 1, y: 1}}
                    style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        top: 0,
                        height: 50
                    }}
                />
                <Text style={styles.creditsText}>20</Text>
                <Star />
            </View>
        </View>
        {/* <View style={styles.offerWrapper}>
            <Text style={styles.offer}>Coffee 3 credits</Text>
        </View> */}
    </View>
)

export default function HorizontalSlider() {

    const { vendors, fetchVendors } = useVendorsStore(state => state)
    // const fetchVendors = useVendorsStore(state => state.)

    useEffect(() => {
        fetchVendors()
    }, [])

    return (
        vendors ?
            <FlatList
                style={styles.slider}
                data={vendors}
                renderItem={renderCard} 
                horizontal
                keyExtractor={item => item.id}
            />
        :
            <Text>Loading</Text>
    )
}


const styles = StyleSheet.create({
    slider: {
        marginHorizontal: -10
    },
    cardWrapper: {
        paddingVertical: 10,
        marginHorizontal: 10,
        shadowOffset: { width: 0, height: 2},
        shadowOpacity: 0.10,
        shadowRadius: 5,
        shadowColor: '#000',
    },
    card: {
        borderRadius: 10,
        overflow: 'hidden',
        backgroundColor: 'white',
        elevation: 2
    },
    image: {
        width: 220,
        height: 220
    },
    nameOverlay: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
    },
    name: {
        padding: 10,
        color: '#fff',
        fontSize: 18,
        fontWeight: '700'
    },
    offerWrapper: {
        padding: 10
    },
    credits: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        padding: 10,
        flexDirection: 'row-reverse',
        alignItems: 'center'
    },
    creditsText: {
        fontSize: 14,
        marginLeft: 5,
        color: '#fff',
        fontWeight: '600'
    }
})