import React from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';

export const Input = ({ key, value, onChangeValue, ...props }) => {

    return (
      <TextInput
        {...props}
        value={value}
        onChangeText={(text) => onChangeValue(key, text)}
        style={styles.inputField}
      />
    )
}

const styles = StyleSheet.create({
  inputField: {
    width: '100%',
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 5,
    padding: 10
  },
})