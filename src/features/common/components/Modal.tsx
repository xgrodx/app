import React from 'react'
import { View, Text, Button, StyleSheet } from 'react-native'

export const Modal = ({children, navigation}) => {

    return (
        <View style={styles.container}>
            <View style={styles.modal}>
                <View style={styles.slot}>{children}</View>
                <Button onPress={() => navigation.goBack()} title="Dismiss" />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 15,
        paddingTop: 60,
    },
    modal: { 
        flex: 1,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        alignItems: 'center',
        justifyContent: 'center' ,
        backgroundColor: '#ffffff',
        padding: 15,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,

    },
    slot: {
        width: '100%'
    }
})