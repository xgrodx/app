import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack';
import { FontAwesome } from '@expo/vector-icons'; 
import Home from '../../home/screens/Home';
import ProfileScreen from '../../auth/screens/Profile';
import RecycleScreen from '../../recycling/screens/Recycling';
import * as React from 'react';
import { StyleSheet, View } from 'react-native';

const BottomTab = createBottomTabNavigator()

const TabBarIcon = (props: { name: string; color: string }) => {
  // return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
  return <FontAwesome size={24} {...props} />
}

const LargeIcon = (props) => {
  return (
    <View style={styles.largeIcon}>
      <FontAwesome size={34} {...props} />
    </View>
  )
}

const styles = StyleSheet.create({
  largeIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#518F81',
    borderRadius: 1000,
    width: 60,
    height: 60,
    marginTop: -35,
    shadowOffset: {width: 0, height: 5},
    shadowRadius: 3,
    shadowOpacity: 0.25,
    elevation: 3
  }
})

const BottomTabNavigator = () => {

  return (
    <BottomTab.Navigator initialRouteName="Home" tabBarOptions={{ showLabel: false }}>
      <BottomTab.Screen
        name="Home"
        component={HomeTabNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="home" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Recycle"
        component={RecycleTabNavigator}
        options={{
          tabBarIcon: ({ color }) => <LargeIcon name="qrcode" color='#fff' />,
        }}
      />
      <BottomTab.Screen 
        name="Profile"
        component={ProfileTabNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="user" color={color} />,
        }}
      />
    </BottomTab.Navigator>
  )
}

const HomeTabStack = createStackNavigator()
const HomeTabNavigator = () =>
    <HomeTabStack.Navigator headerMode="none">
      <HomeTabStack.Screen
        name="Home"
        component={Home}
        options={{ headerTitle: 'Home Screen' }}
      />
    </HomeTabStack.Navigator>


const RecycleTabStack = createStackNavigator()
const RecycleTabNavigator = () =>
    <RecycleTabStack.Navigator>
      <RecycleTabStack.Screen
        name="Recycle"
        component={RecycleScreen}
        options={{ headerTitle: 'Scan QR Code' }}
      />
    </RecycleTabStack.Navigator>


const ProfileTabStack = createStackNavigator()
const ProfileTabNavigator = () =>
    <ProfileTabStack.Navigator>
      <ProfileTabStack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{ headerTitle: 'Profile' }}
      />
    </ProfileTabStack.Navigator>

export default BottomTabNavigator