import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createStackNavigator, StackNavigationOptions } from '@react-navigation/stack';
import * as React from 'react';
import { ColorSchemeName } from 'react-native';

import NotFound from './screens/NotFound';
import { RootStackParamList } from '../../../../types';
import BottomTabNavigator from './BottomTabNavigator';
import LinkingConfiguration from './LinkingConfiguration';
import { AuthModal } from '../../auth/components/AuthModal';


const Theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: '#E5D2B9',
    primary: '#518F81',
  },
};

// If you are not familiar with React Navigation, we recommend going through the
// "Fundamentals" guide: https://reactnavigation.org/docs/getting-started
export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      // theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      theme={Theme}>
      <RootNavigator />
    </NavigationContainer>
  );
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();

function RootNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{ headerShown: false }}
      mode="modal"
      // headerMode='none'
      // cardStyle={{ backgroundColor: 'transparent', opacity: 0.9 }}
    >
      <Stack.Screen name="Root" component={BottomTabNavigator} />
      <Stack.Screen name="NotFound" component={NotFound} options={{ title: 'Oops!' }} />

      <Stack.Screen
        name="Modal"
        component={AuthModal}
        options={{ 
          cardStyle: {
            opacity: 0.9,
            backgroundColor: 'transparent'
          },
          gestureResponseDistance: { vertical: 1000 },
        }}
      />
    </Stack.Navigator>
  );
}