import * as React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { getActivity } from '../../auth/flows/user'
import Star from './Star'
import Button from '../../common/components/Button'

const activity = getActivity()

const Item = ({item}) => (
    <View style={styles.item}>
        <View style={{flexDirection: 'row'}}>
            <Image style={styles.image} source={item.image} />
            <View>
                <Text style={styles.name}>{item.name}</Text>
                <Text style={styles.smallText}>{item.restaurant}</Text>
                <Text style={styles.smallText}>{item.date}</Text>
            </View>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Star style={styles.star} />
            <Text style={styles.smallText}>{item.credits} credits</Text>
        </View>
    </View>
)

export default function Activity() {
    return (
        <View style={styles.container}>
            {activity.map((item, i) => <Item key={i} item={item} />)}
            {/* <View style={{marginTop: 10}}>
                <Button title="View all" />
            </View> */}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        padding: 10,
        borderRadius: 5,
        shadowOffset: {width: 0, height: 3},
        shadowOpacity: 0.2,
        elevation: 3
    },
    image: {
        width: 50,
        height: 50,
        resizeMode: "contain",
        marginRight: 10
    },
    item: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: '#eee',
        borderBottomWidth: 1,
        paddingVertical: 10
    },
    name: {
        fontSize: 16,
        fontWeight: '600',
        // marginBottom: 3
    },
    smallText: {
        color: '#666',
    },
    star: {
        marginRight: 2
    }
})