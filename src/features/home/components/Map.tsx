import * as React from 'react'
import { Dimensions, StyleSheet, View } from "react-native";
import MapView, { Marker } from 'react-native-maps';
import { getRestaurants } from '../../vendors/flows/vendors';
import { getRecyclingStations } from '../../recycling-stations/flows/recycling'

const markers = getRecyclingStations()

const Mark = ({marker}) => (
    <Marker
      key={marker.id}
      coordinate={marker.latlng}
      title={marker.name}
      description={marker.description}
    />
)

export default function Map() {
    return (
        <View style={styles.container}>
            <View style={{borderRadius: 5, overflow: 'hidden'}}>
                <MapView style={styles.mapStyle}
                    initialRegion={{
                        latitude: 59.2974415,
                        longitude: 17.9961514,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,

                    }}
                >
                    { markers.map((m,i) => <Mark key={i} marker={m} /> )}
                </MapView>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        elevation: 3,
        shadowOffset: { width: 0, height: 2},
        shadowOpacity: 0.2,
        shadowRadius: 5,
    },
    mapStyle: {
        alignSelf: 'stretch',
        height: 250,
    }
})