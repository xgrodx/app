import * as React from 'react'
import { Image, StyleSheet, Text, View } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';
import { userStore } from '../../auth/flows/user'

export default function UserInfo() {

    const user = userStore(state => state.user)

    console.log('USER', user)

    return (
        <View style={styles.container}>

            <View style={{alignSelf: 'stretch'}}>
                <LinearGradient
                    // Background Linear Gradient
                    colors={['#56A2A6', '#518F81']}
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 1}}
                    style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                    }}
                />

                <View style={styles.flex}>
                    {user ? 
                        <>
                            <Image style={styles.avatar} source={{uri: user.photoURL }} />
                            <Text style={styles.userName}>{user.displayName}</Text>
                            <Text style={styles.city}>Stockholm</Text>
                        </>
                    : 
                        <>
                            <Image style={styles.avatar} source={{uri: 'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=3&w=934&q=80' }} />
                        </>
                    }
                </View>
            </View>

            <View style={styles.pill}>
                <View style={[styles.borderRight, styles.pillContent]}>
                    <Text style={styles.number}>56</Text>
                    <Text style={styles.smallText}>credits</Text>
                </View>
                <View style={styles.pillContent}>
                    <Text style={styles.number}>23</Text>
                    <Text style={styles.smallText}>recycled items</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    flex: {
        paddingVertical: 50,
        paddingTop: 60,
        display: 'flex',
        alignItems: 'center',
    },
    userInfo: {
        marginLeft: 15,
        justifyContent: 'center'
    },
    avatar: {
        width: 100,
        height: 100,
        borderRadius: 50,
        marginBottom: 10
    },
    userName: {
        fontSize: 18,
        marginBottom: 3,
        fontWeight: '600',
        color: '#fff',
        alignSelf: 'stretch',
        textAlign: 'center',
    },
    city: {
        fontSize: 14,
        color: '#ddd'
    },

    pill: {
        backgroundColor: '#fff',
        height: 50,
        marginTop: -25,
        borderRadius: 25,
        flexDirection: 'row',
        alignItems: 'center',
        shadowRadius: 3,
        shadowOffset: {width: 0, height: 3},
        shadowOpacity: 0.1,
        elevation: 3,
    },
    borderRight: {
        borderRightColor: '#ccc',
        borderRightWidth: 1
    },
    pillContent: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    number: {
        fontSize: 16,
        fontWeight: '500',
        marginRight: 4
    },
    smallText: {
        fontSize: 12,
        color: '#666',
    }
})