import * as React from 'react'
import { View, Text, StyleSheet, Image } from "react-native";
import { ScrollView } from 'react-native-gesture-handler';
import Activity from '../components/Activity'
import HorizontalSlider from '../../common/components/HorizontalSlider';
import Map from '../components/Map';
import UserInfo from '../components/UserInfo';

export default function Home() {
    return (
        <ScrollView>
            <View style={{marginBottom: 30}}>
                <UserInfo />
            </View>

            <View style={styles.screen}>

                <View style={{marginBottom: 30}}>
                    <Text style={styles.header}>Redeem Credits</Text>
                    <HorizontalSlider />
                </View>

                <View style={{marginBottom: 40}}>
                    <Text style={[styles.header, {marginBottom: 10}]}>Nearby recycling stations</Text>
                    <Map />
                </View>

                <View style={{marginBottom: 30}}>
                    <Text style={[styles.header, {marginBottom: 10}]}>Your activity</Text>
                    <Activity />
                </View>

            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    screen: {
        padding: 10
    },
    header: {
        fontSize: 20,
        fontWeight: '700',
        color: '#444',
        textTransform: 'uppercase'
    }
})