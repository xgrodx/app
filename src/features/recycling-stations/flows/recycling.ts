export const getRecyclingStations = () => [
    // Mock data
    {
        id: '1',
        name: 'Hökmossevägen 20',
        description: 'Lorem ipsum dolor sit amet',
        latlng: { 
            latitude: 59.292222,
            longitude: 17.9936075
        }
    },
    {
        id: '2',
        name: 'Stora Coop Västberga',
        description: 'Lorem ipsum dolor sit amet',
        latlng: { 
            latitude: 59.2931095,
            longitude: 18.0039179
        }
    }


]