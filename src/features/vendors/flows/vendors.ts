import create from 'zustand'
import { firestore } from '../../../app/firebase'

// const fetchVendors = async () => {
//     const vendors = await firestore().collection('vendors').get()
//     return vendors
// }

//     const vendors = await firestore().collection('vendors').get()

//     // Mock data
//     {
//         id: '1',
//         latlng: {latitude: 59.2974415, longitude: 17.9961514},
//         name: 'The Juice Bar',
//         description: 'Restaurang Landet',
//         image: 'https://images.unsplash.com/photo-1563245374-27386492c152?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2537&q=80',
//     },
//     {
//         id: '2',
//         latlng: {latitude: 59.2974415, longitude: 17.9961514},
//         name: 'Svenska Sushiköket', 
//         description: 'Svensk sushi',
//         image: 'https://images.unsplash.com/photo-1502364271109-0a9a75a2a9df?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2550&q=80',
//     },
//     {
//         id: '3',
//         latlng: {latitude: 59.2975415, longitude: 17.9961514},
//         name: 'Adam & Siam',
//         description: 'Restaurang Landet',
//         image: 'https://images.unsplash.com/photo-1553365857-9cc1a715084f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1756&q=80',
//     },
//     {
//         id: '4',
//         latlng: {latitude: 59.2974415, longitude: 17.9961514},
//         name: 'Mel\'s Diner', 
//         description: 'Restaurang Landet',
//         image: 'https://images.unsplash.com/photo-1589684533981-aab83b80bc18?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2167&q=80',
//     },
//     {
//         id: '5',
//         latlng: {latitude: 59.2974415, longitude: 17.9961514},
//         name: 'Ask Italian',
//         description: 'Restaurang Landet',
//         image: 'https://images.unsplash.com/photo-1597402542026-7a720faf4a15?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1253&q=80',
//     },
//     {
//         id: '6',
//         latlng: {latitude: 59.2974415, longitude: 17.9961514},
//         name: 'Trailboss Burgers', 
//         description: 'Svensk sushi',
//         image: 'https://images.unsplash.com/photo-1519167874178-f1c834c38f25?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2550&q=80',
//     },
// ]

const useVendorsStore = create(set => ({
    vendors: [],
    fetchVendors: async () => {
        const snapshot = await firestore.collection('vendors').get()
        const vendors = snapshot.docs.map(doc => ({id: doc.uid, ...doc.data()}))
        set({vendors})
    },
}))

export { useVendorsStore }