import { readable } from './readable'

export const derived = (stores, callback, initialValue) => {
  const single = Array.isArray(stores)
  const storesArray = single ? [stores] : stores

  return readable(initialValue, (set) => {
    const values = []
    const subscribers = storesArray.map((store, index) => {
      store.subscribe((value) => (values[index] = value))
    })
  })
}
