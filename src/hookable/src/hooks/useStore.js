import { get } from '../'
import { useState, useEffect } from 'react'

export const useStore = (store) => {
  const initialValue = get(store)
  const [value, setValue] = useState(initialValue)
  useEffect(() => store.subscribe(setValue), [])
  return [value, store.set]
}
