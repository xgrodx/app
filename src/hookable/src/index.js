export const get = (store) => {
  let value
  store.subscribe(store, (state) => (value = state))()
  return value
}
