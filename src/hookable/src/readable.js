import { writable } from './writable'

export const readable = (initialValue) => ({
  subscribe: writable(initialValue).subscribe,
})
