export const writable = (initialState) => {
  let state = initialState
  let subscribers = []

  const notifySubscribers = (state) => {
    subscribers.forEach((subscriber, index, object) => {
      try {
        subscriber(state)
      } catch {
        // Callback failed - so we remove it
        object.splice(index, 1)
      }
    })
  }

  const set = (value) => {
    state = value
    notifySubscribers(state)
  }

  const update = (fn) => set(fn(state))

  return {
    set,
    update,

    subscribe: (fn, initialCallback) => {
      subscribers = [...subscribers, fn]
      if (initialCallback) initialCallback(state)

      // Unsubscribe
      return () => subscribers.splice(subscribers.indexOf(fn), 1)
    },

    unsubscribe: (fn) => {
      subscribers = subscribers.filter((subscriber) => subscriber !== fn)
    },
  }
}
