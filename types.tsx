export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
  Modal: undefined;
};

export type BottomTabParamList = {
  Home: undefined;
  Recycle: undefined;
  Profile: undefined;
};

export type HomeTabParamList = {
  HomeScreen: undefined;
};

export type RecycleTabParamList = {
  RecycleScreen: undefined;
};
export type ProfileTabParamList = {
  ProfileScreen: undefined;
};
